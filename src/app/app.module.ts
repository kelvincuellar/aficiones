import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AficionService } from './aficion.service';
import { AppComponent } from './app.component';
import { AficionesAddComponent } from './aficiones-add/aficiones-add.component';
import { AficionesEditComponent } from './aficiones-edit/aficiones-edit.component';
import { AficionesGetComponent } from './aficiones-get/aficiones-get.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  //Agregar los diferentes componentes de la app
  declarations: [
    AppComponent,
    AficionesAddComponent,
    AficionesEditComponent,
    AficionesGetComponent
  ],
  //importar los modulos requeridos en la app
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  //Registrar los servicios (Controladores encargados de aplicar el ruteo)
  providers: [AficionService],
  //El componente principal de la app
  bootstrap: [AppComponent]
})
export class AppModule { }
