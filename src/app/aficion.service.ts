import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AficionService {

  /*URL para la mongodb*/
  uri = 'http://localhost:4000/aficiones';

  //Agregamos en el, el cliente para conectarse a la mongodb
  constructor(private http: HttpClient) { }

  /*Fn: Agrega la aficion como registro */
  agregar(nombre: string, descripcion: string) {
    const af = {
      nombre,
      descripcion
    };
    //console.log(af);

    /*Fn: Realizar la peticion post, para realizar el registro.*/
    this.http.post(`${this.uri}/add`, af).subscribe(resultado => console.log('Listo'));
  }

  /*Fn: Obtener el listado de aficiones*/
  getAficiones() {
    return this.http.get(`${this.uri}`);
  }

  /*Fn: Obtener la aficion seleccionada*/
  editAficion(id) {
    return this.http.get(`${this.uri}/edit/${id}`);
  }

  /*Fn: Actualizar aficion*/
  actAficion(nombre, descripcion, id) {
    const ax = { nombre, descripcion };
    this.http.post(`${this.uri}/update/${id}`, ax).subscribe(res => console.log('Actualizado'));
  }

  eliminarAficion(id){
    return this.http.get(`${this.uri}/delete/${id}`);
  }
}
