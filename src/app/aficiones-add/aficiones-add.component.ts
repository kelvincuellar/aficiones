import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AficionService } from '../aficion.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-aficiones-add',
  templateUrl: './aficiones-add.component.html',
  styleUrls: ['./aficiones-add.component.css']
})
export class AficionesAddComponent implements OnInit {

  angForm: FormGroup;
  /*Agregamos un constructor que reciba el servicio y el ruteo de la app*/
  constructor(private afserv: AficionService, private router: Router) {
    this.angForm = new FormGroup({
      nombre: new FormControl(),
      descripcion: new FormControl()
    });
  }

  /*Fn: Metodo encargado de agregar una aficion del usuario*/
  agregarAficion(nombre: string, descripcion: string) {
    this.afserv.agregar(nombre, descripcion);
    this.router.navigate(['afics']);
    //console.log("Within: "+nombre+", "+descripcion);
  }

  /*Este metodo se ejecuta cada vez que se renderiza el componente, es void ya que no devuelve nada*/
  ngOnInit(): void {
  }
}
