import { NgModule } from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';
import { AficionesAddComponent } from './aficiones-add/aficiones-add.component';
import { AficionesEditComponent } from './aficiones-edit/aficiones-edit.component';
import { AficionesGetComponent } from './aficiones-get/aficiones-get.component';

const routes: Routes = [
  {
    path: 'afics/create',
    component: AficionesAddComponent
  },
  {
    path: 'edit/:id',
    component: AficionesEditComponent
  },
  {
    path: 'afics',
    component: AficionesGetComponent
  }
];


@NgModule({
  imports: [CommonModule,RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
