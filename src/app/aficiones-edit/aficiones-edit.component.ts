import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AficionService } from '../aficion.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-aficiones-edit',
  templateUrl: './aficiones-edit.component.html',
  styleUrls: ['./aficiones-edit.component.css']
})
export class AficionesEditComponent implements OnInit {

  angForm: FormGroup;
  aficion: any = {};

  //Utilizamos variables de ruteo, e iniciamos nuestro formulario emparejando la estructura de datos
  //PD: Importansitimo agregar la definicion del formulario
  constructor(private rout: ActivatedRoute, private route: Router, private afServ: AficionService) {
    this.angForm = new FormGroup({
      nombre: new FormControl(),
      descripcion: new FormControl() 
   });
  }

  /*Fn: Actualizamos los campos del formulario*/
  actAficion(nombre, descripcion) {
    this.rout.params.subscribe(params => {
      this.afServ.actAficion(nombre, descripcion, params.id);
      this.route.navigate(['afics']);
    });
  }

  //Inicializamos la seleccion realizada como usuario
  ngOnInit(): void {
    this.rout.params.subscribe(params => {
      this.afServ.editAficion(params[`id`]).subscribe(res => { this.aficion = res });
    });
  }
}
