import { Component, OnInit } from '@angular/core';
import Aficion from '../Aficion';
import { AficionService } from '../aficion.service';

@Component({
  selector: 'app-aficiones-get',
  templateUrl: './aficiones-get.component.html',
  styleUrls: ['./aficiones-get.component.css']
})
export class AficionesGetComponent implements OnInit {

  listado: Aficion[];

  constructor(private afserv: AficionService) { }

  borrarAficion(id, indice) {
    this.afserv.eliminarAficion(id).subscribe(res => {
      this.listado.splice(indice,1);
    });
  }

  ngOnInit(): void {
    this.afserv.getAficiones().subscribe((data: Aficion[]) => { this.listado = data; });
  }
}
