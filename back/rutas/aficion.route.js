//Archivo de rutas para la entidad Aficion

const express = require('express');
const app = express();
const afRoutes = express.Router();

//Hacemos referencia a la entidad
let Aficion = require('../modelos/Aficion');

//Ruta de registro
afRoutes.route('/add').post(function (req, res) {
    let af = new Aficion(req.body);
    console.log('Aparentemente');
    af.save()
        .then(af => {
            res.status(200).json({ 'Aficion': 'Aficion agregada correctamente' });
        })
        .catch(ex => {
            res.status(400).send('No se pudo guardar el registro');
        });
});


//Ruta de listados
afRoutes.route('/').get(function (req, res) {
    Aficion.find(function (error, aficiones) {
        if (error) {
            console.log(error);
        } else {
            res.json(aficiones);
        }
    });
});

//Ruta de obtencion de entidad
afRoutes.route('/edit/:id').get(function (req, res) {
    let id = req.params.id;
    Aficion.findById(id, function (error, aficion) {
        res.json(aficion);
    });
});

//Ruta de edicion
afRoutes.route('/update/:id').post(function (req, res) {
    Aficion.findById(req.params.id, function (erro, aficion) {
        if (!aficion) {
            res.status(404).send('Registro incorrecto');
        } else {
            aficion.nombre = req.body.nombre;
            aficion.descripcion = req.body.descripcion;

            aficion.save()
                .then(aficion => {
                    res.json('Actualizacion completa');
                })
                .catch(erro => {
                    res.status(400).send('No es posible actualizar el registro en BD');
                });
        }
    });
});

//Ruta para eliminacion
afRoutes.route('/delete/:id').get(function (req, res) {
    Aficion.findByIdAndRemove({ _id: req.params.id }, function (err, aficion) {
        if (err) {
            res.json(err);
        } else {
            res.json('Eliminado satisfactoriamente');
        }
    });
});

module.exports = afRoutes;