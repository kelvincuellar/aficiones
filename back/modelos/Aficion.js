//Entidad Aficion para BD
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Aficion = new Schema({
    nombre: { type: String },
    descripcion: { type: String }
},
{
    collection: 'Aficion'
});

module.exports = mongoose.model('Aficion', Aficion);