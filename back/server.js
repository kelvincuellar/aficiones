//Servidor NODE
const express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    conex = require('./db'),
    afRoute = require('./rutas/aficion.route');

/*Conexion a la base de datos*/
mongoose.Promise = global.Promise;
mongoose.connect(conex.DB, { useUnifiedTopology: true, useNewUrlParser: true }).then(
    () => { console.log('Base conectada') },
    ex => { console.log('Error al conectar BD: ' + ex) }
);

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use('/aficiones', afRoute);

let port = process.env.PORT || 4000;

const server = app.listen(port,function () {
    console.log('Corriendo server en puerto: ' + port);
});